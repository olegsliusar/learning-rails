# Learn Ruby on [<img src="http://rubyonrails.org/images/rails-logo.svg" width="140" alt="Rails">](http://rubyonrails.org/)

> New to Rails? See [What is Ruby on Rails](http://railsapps.github.io/what-is-ruby-rails.html). 

<br>

## Table of Contents

* [Interactive Tutorials](#interactive-tutorials)
* [Bootcamps & Mentorship](#bootcamps--mentorship)
* [Books](#books)
  - [Beginner](#beginner)
  - [Intermediate](#intermediate)
  - [Advanced](#advanced)
* [Videos](#videos)
* [Tutorials](#tutorials)
* [Courses](#courses)
* [Learn with Code](#learn-with-code)
* [Code Challenges and Puzzles](#code-challenges-and-puzzles)
* [Help](#help)
* [Community](#community)
* [News](#news)
* [Stay Up to Date](#stay-up-to-date)
* [Podcasts](#podcasts)
* [Screencasts](#screencasts)
* [Talks](#talks)
* [Style Guides](#style-guides)
* [Third-party APIs](#third-party-apis)
* [Install Rails](#install-rails)
* [DevTools](#devtools)
* [Built with Rails](#built-with-rails)

---

#### If you're starting from scratch or have no idea what to choose, I highly recommend you go to [The Odin Project][top]. It's a free online curriculum, from zero to getting hired.

<br>

---

## Interactive Tutorials
Name | From | Access
:--  | :--  | :--:
[Learn Ruby](https://www.codeschool.com/learn/ruby) | [CodeSchool][cs] | Mostly Paid
[Ruby](https://www.codecademy.com/tracks/ruby) | [Codecademy][cc] | Mostly Free
[Learn Ruby on Rails](https://www.codecademy.com/courses/learn-rails) | [Codecademy][cc] | Mostly Paid

---

## Bootcamps & Mentorship
Name | From | Access
:--  | :--  | :--:
[Become a Web Developer][vcs] | [Viking Code School][vcst] | Mostly Paid
[Online Programming Bootcamp for Serious Web Developers][ls] | [Launch School][ls] | Paid
[Online mentorship to advance your career][t]|[Thinkful][t]|Paid
[Firehose Project][tfp] |[Firehose][tfp]|Paid
[Learn Rails Fundamentals][bwdb]|[Bloc][b]|Paid
[Online Courses in Web Development][bwdct]|[Bloc][b]|Paid

---

## Books

### Beginner
1. [Developer Fundamentals][lewdf] by [Learn Enough][lea] (+Screencasts)
1. [Learn to Program](https://pine.fm/LearnToProgram/)
1. [Why's (Poignant) Guide to Ruby](http://mislav.uniqpath.com/poignant-guide/)
1. [Programming Ruby 1.9 & 2.0](https://pragprog.com/book/ruby4/programming-ruby-1-9-2-0)
1. [The Ruby Programming Language](http://shop.oreilly.com/product/9780596516178.do)
1. [Learn Ruby The Hard Way](http://learnrubythehardway.org/)
1. [Learn Ruby on Rails (Daniel Kehoe)](http://learn-rails.com/learn-ruby-on-rails.html)
1. [Ruby on Rails Tutorial](https://www.railstutorial.org/)
1. [Ruby on Rails Guides](http://guides.rubyonrails.org/) by [Rails Community](http://guides.rubyonrails.org/credits.html)
1. [Agile Web Development with Rails 5](https://pragprog.com/book/rails5/agile-web-development-with-rails-5)

### Intermediate

1. [Practical Object-Oriented Design in Ruby](http://www.poodr.com/)
1. [The Well-Grounded Rubyist](https://www.manning.com/books/the-well-grounded-rubyist)
1. [Rails 4 in Action](https://www.manning.com/books/rails-4-in-action)

### Advanced

1. [Crafting Rails 4 Applications](https://pragprog.com/book/jvrails2/crafting-rails-4-applications)
1. [Metaprogramming Ruby 2](https://pragprog.com/book/ppmetr2/metaprogramming-ruby)
1. [Rails AntiPatterns: Best Practice Ruby on Rails Refactoring](http://www.amazon.com/Rails-AntiPatterns-Refactoring-Addison-Wesley-Professional/dp/0321604814)
1. [The Rails 5 Way](https://leanpub.com/tr5w)
1. [Eloquent Ruby](http://eloquentruby.com/)
1. [Design Patterns in Ruby](http://designpatternsinruby.com/)

---

## Videos

Name | From | Access
:--  | :--  | :--:
[Top Developer Training](https://pragmaticstudio.com/refs/railstutorial)|[The Pragmatic Studio](https://pragmaticstudio.com/about) | Paid
[Learn Rails Development][tthtrd] |[Treehouse][tth] | Paid
[Rails Courses](https://mackenziechild.me/rails-courses/) | [Mackenzie Child](https://mackenziechild.me/) | Free
[One Month Rails][omcomr] | [One Month][om] | Paid
[Agile Development Using Ruby on Rails - Basics](https://www.edx.org/course/agile-development-using-ruby-rails-uc-berkeleyx-cs169-1x) | [edX](https://www.edx.org/) | Free

---

## Tutorials

1. [Capstone Rails Tutorials](https://tutorials.railsapps.org/)
1. [How to develop a social network using Ruby on Rails](https://medium.com/rails-ember-beyond/how-to-build-a-social-network-using-rails-eb31da569233)
1. [SaaS Course - The welcome word - Prograils](https://prograils.com/courses/saas/ch/01-the-welcome-word)
1. [Facebook Authentication in Ruby on Rails](https://richonrails.com/articles/facebook-authentication-in-ruby-on-rails)
1. [Google Authentication in Ruby On Rails](https://richonrails.com/articles/google-authentication-in-ruby-on-rails)
1. [Gmail Like Chat Application in Ruby on Rails](http://josephndungu.com/tutorials/gmail-like-chat-application-in-ruby-on-rails)
1. [Private Inbox System in Rails with Mailboxer](http://josephndungu.com/tutorials/private-inbox-system-in-rails-with-mailboxer)

---

## Courses
Name | Access |From
:--  | :--:   |:--:
[Web Application Development: Basic Concepts][clwa]|Mostly Free|[Coursera][c]

---

## Learn with Code

1. [RailsApps Example Applications](http://railsapps.github.io/) - Code from the [Capstone Rails Tutorials](https://tutorials.railsapps.org/)
1. [Treebook](https://github.com/jasonseifer/treebook) - A social networking web application with features similar to Facebook.
1. [Socify](https://github.com/sudharti/socify) - Socify is an open source social networking platform written in Ruby on Rails.
1. [Sample App](https://github.com/mhartl/sample_app_3rd_edition) - The sample app for the 3rd edition of the Ruby on Rails Tutorial.
1. [Pinteresting](https://github.com/onemonthrails/pinteresting) - This is the pinteresting sample application for One Month Rails
1. [MyRecipes](https://github.com/railsudemycourse/myrecipes) -A recipe app written in Ruby on Rails.
1. [Learn Ruby with the Edgecase Ruby Koans][rk] by [Jim Weirich](https://twitter.com/jimweirich)

---

## Code Challenges and Puzzles

* [HackerRank](https://www.hackerrank.com/domains/ruby)
* [CodeWars](https://www.codewars.com/)
* [Exercisme.io](http://exercism.io/languages/ruby)

---

## Help

* [StackOverflow](http://stackoverflow.com/questions/tagged/ruby-on-rails)
* [Chatroom (Browser-based IRC)](http://webchat.freenode.net/?channels=rubyonrails)
* [Reddit](https://www.reddit.com/r/rails)

---

## Community

- [Ruby on Rails: Community](http://rubyonrails.org/community/)
- [Ruby on Rails: Talk - Google group](https://groups.google.com/forum/#!forum/rubyonrails-talk)
- [Ruby Community](https://www.ruby-lang.org/en/community/)
- [RubyFlow - The Ruby and Rails community linklog](http://www.rubyflow.com/)

---

## News

- [Riding Rails](http://weblog.rubyonrails.org/)
- [Ruby Weekly](http://rubyweekly.com/)
- [Ruby Flow](http://www.rubyflow.com/)

---

## Stay Up to Date

- [@rails](https://twitter.com/rails): The official Ruby on Rails Twitter account. Follow them if you want insight on release dates, and development.
- [@rails_apps](https://twitter.com/rails_apps): Daniel Kehoe is posting new Rails example apps pretty often, pretty useful.
- [@dhh](http://twitter.com/dhh): The creator of Ruby on Rails. Posts insight on new releases and various tips.
- [@rbates](https://twitter.com/rbates): Producer of Railscasts, posts tips and tricks very often.

---

## Podcasts

- [Ruby5](https://ruby5.codeschool.com/)
- [Ruby Rogues](https://devchat.tv/ruby-rogues/)
- [5by5](http://5by5.tv/rubyonrails)

---

## Screencasts

1. [RailsCasts: Ruby on Rails Screencasts](http://railscasts.com/)
1. [GoRails](https://gorails.com/)

---

## Talks

1. [The Best of RailsConf 2015](https://mattbrictson.com/best-of-railsconf-2015)
1. [Railsconf 2014, Talks I Liked](http://nepalonrails.com/blog/2014/05/railsconf-2014-talks-i-liked)
1. [Favorite talks of RailsConf 2013](http://blog.planetargon.com/entries/2013/5/31/favorite-talks-of-railsconf-2013)

---

## Style Guides

- [A community-driven Ruby coding style guide](https://github.com/bbatsov/ruby-style-guide)
- [A community-driven Rails 3 & 4 style guide](https://github.com/bbatsov/rails-style-guide)
- [GitHub Ruby Coding Style](https://github.com/styleguide/ruby)
- [Thoughtbot Style Guides](https://github.com/thoughtbot/guides)

---

## Third-party APIs

* [twilio-ruby](https://github.com/twilio/twilio-ruby) - A module for using the Twilio REST API and generating valid TwiML.
* [twitter](https://github.com/sferik/twitter) - A Ruby interface to the Twitter API.
* [wikipedia](https://github.com/kenpratt/wikipedia-client) - Ruby client for the Wikipedia API.
* [databasedotcom](https://github.com/heroku/databasedotcom) - Ruby client for the Salesforce's Database.com and Chatter APIs.
* [Dropbox](https://github.com/futuresimple/dropbox-api) - Dropbox API Ruby Client.
* [facy](https://github.com/huydx/facy) - Command line power tool for facebook.
* [fb_graph](https://github.com/nov/fb_graph) - A full-stack Facebook Graph API wrapper.
* [flickr](https://github.com/RaVbaker/flickr) - A Ruby interface to the Flickr API.
* [gitlab](https://github.com/NARKOZ/gitlab) - Ruby wrapper and CLI for the GitLab API.
* [gmail](https://github.com/gmailgem/gmail) - A Rubyesque interface to Gmail, with all the tools you'll need.
* [hipchat-rb](https://github.com/hipchat/hipchat-rb) - HipChat HTTP API Wrapper in Ruby with Capistrano hooks.
* [instagram-ruby-gem](https://github.com/Instagram/instagram-ruby-gem) - The official gem for the Instagram REST and Search APIs.
* [itunes_store_transporter](https://github.com/sshaw/itunes_store_transporter) - Ruby wrapper around Apple's iTMSTransporter program.
* [linkedin](https://github.com/hexgnu/linkedin) - Provides an easy-to-use wrapper for LinkedIn's REST APIs.
* [Octokit](http://octokit.github.io/octokit.rb) - Ruby toolkit for the GitHub API.
* [Pusher](https://github.com/pusher/pusher-gem) - Ruby server library for the Pusher API.
* [ruby-gmail](https://github.com/dcparker/ruby-gmail) - A Rubyesque interface to Gmail.
* [ruby-trello](https://github.com/jeremytregunna/ruby-trello) - Implementation of the Trello API for Ruby.
* [Slack ruby gem](https://github.com/aki017/slack-ruby-gem) - A Ruby wrapper for the Slack API.
* [soundcloud-ruby](https://github.com/soundcloud/soundcloud-ruby) - Official SoundCloud API Wrapper for Ruby.
* [Yt](https://github.com/Fullscreen/yt) - An object-oriented Ruby client for YouTube API V3.
* [t](https://github.com/sferik/t) - A command-line power tool for Twitter.
* [tweetstream](https://github.com/tweetstream/tweetstream) - A simple library for consuming Twitter's Streaming API.

---

## Install Rails

* [Install Ruby on Rails 5.0](http://railsapps.github.io/installing-rails.html) by [RailsApps](http://railsapps.github.io/)
* [RailsBridge Installfest](http://installfest.railsbridge.org/installfest/)
* [GoRails](https://gorails.com/setup)
* [Rails OS X Setup Guide](https://mattbrictson.com/rails-osx-setup-guide) by [Matt Brictson](https://mattbrictson.com/)

> If you've run into issues with your installation and are desperately looking for something else to try, take a deep breath first and go back over the instructions step-by-step to make sure you've followed them properly. You can run into some odd issues if you start trying to mix together different installation recommendations, because some of them use auto-installers and have you install things in slightly different places so you may end up with a couple copies of key components. It may work fine on the surface, but some day it'll probably come back and frustrate you again.

---

## DevTools

- [Atom](http://www.atom.io)
  - [Atom Documentation](https://atom.io/docs)
- [Sublime Text](http://www.sublimetext.com/)
  - [Sublime Text 3 Documentation](https://www.sublimetext.com/docs/3/)
  - [Setting up Sublime Text 3 for Rails Development](https://mattbrictson.com/sublime-text-3-recommendations) - [Matt Brictson](https://mattbrictson.com/)
- [RubyMine](https://www.jetbrains.com/ruby/)
- [Vim](http://www.vim.org/)

---

## Built with Rails

1. [Twitter](https://twitter.com/)
1. [Github](https://github.com/)
1. [Basecamp](https://basecamp.com/)
1. [Shopify](http://www.shopify.com/)
1. [Crunchbase](https://www.crunchbase.com/)
1. [Slideshare](http://www.slideshare.net/)
1. [AirBnb](https://www.airbnb.com/)
1. [Groupon](http://www.groupon.com/)
1. [SoundCloud](https://soundcloud.com/)
1. [Square](https://squareup.com/)
1. [Heroku](https://www.heroku.com/)
1. [Yellow pages](http://www.yellowpages.com/)
1. [Hulu](http://www.hulu.com/)
1. [Urban Dictionary](http://www.urbandictionary.com/)
1. [CrunchBase](https://www.crunchbase.com/)
1. [Bloomberg](http://www.bloomberg.com/)
1. [Zendesk](https://www.zendesk.com/)
1. [Scribd](https://www.scribd.com/)
1. [Bleacher Report](http://bleacherreport.com/)
1. [500px](http://500px.com/)
1. [Ask.fm](http://ask.fm/)
1. [Dribbble](http://dribbble.com/)
1. [Funny or Die](http://www.funnyordie.com/)
1. [Goodreads](http://www.goodreads.com/)
1. [Indiegogo](http://www.indiegogo.com/)
1. [Kickstarter](http://www.kickstarter.com/)
1. [LivingSocial](http://www.livingsocial.com/)
1. [Lumosity](http://www.lumosity.com/)
1. [MyFitnessPal](http://www.myfitnesspal.com/)
1. [Pixlr](https://pixlr.com/)
1. [Strava](https://www.strava.com/)
1. [Twitch](http://twitch.tv/)
1. [We Heart It](http://weheartit.com/)
1. [Whitepages](http://www.whitepages.com/)
1. [Yammer](https://www.yammer.com)
1. [ThemeForest](http://themeforest.net/)

---

### License

<p xmlns:dct="http://purl.org/dc/terms/">
<a rel="license" href="http://creativecommons.org/publicdomain/mark/1.0/">
<img src="http://i.creativecommons.org/p/mark/1.0/88x31.png"
     style="border-style: none;" alt="Public Domain Mark" />
</a>
<br />
This work (<span property="dct:title">Learning Ruby on Rails</span>), identified by <a href="http://olehsliusar.com/" rel="dct:publisher"><span property="dct:title">Oleh Sliusar</span></a>, is free of known copyright restrictions.
</p>

<!-- links -->

[b]: https://www.bloc.io/
[bwdb]: https://www.bloc.io/web-development-bootcamp/
[bwdct]: https://www.bloc.io/web-developer-career-track
[c]: https://www.coursera.org/
[cc]: https://www.codecademy.com/
[clwa]: https://www.coursera.org/learn/web-app
[cs]: https://www.codeschool.com/
[lea]: https://www.learnenough.com/about/
[lewdf]: https://www.learnenough.com/web-development-fundamentals/
[ls]: http://launchschool.com/railstutorial/
[om]: https://onemonth.com/
[omcomr]: https://onemonth.com/courses/one-month-rails/
[rk]: http://rubykoans.com/
[t]: https://www.thinkful.com/a/railstutorial
[top]: http://www.theodinproject.com/
[topa]: http://www.theodinproject.com/about/
[vcs]: https://www.vikingcodeschool.com/
[vcst]: https://www.vikingcodeschool.com/team/
[tfp]: http://www.thefirehoseproject.com/?tid=HARTL-RAILS-TUT-EB2&pid=HARTL-RAILS-TUT-EB2
[tth]: https://teamtreehouse.com/
[tthtrd]: https://teamtreehouse.com/tracks/rails-development/
